* Vertaisyhteisö opettelemassa etätyötekniikoita

Erilaiset some-alustat (esim. LinkedIn, Twitter ja Facebook) ja pikaviestipalvelut (esim. Slack) ovat loistavia, jos haluat kertoa ajatuksistasi tai esitellä työsi tuloksia julkisesti tai jo järjestäytyneen ryhmän jäsenille. Mutta jos nuo ovat ainoat välineesi ja tavoitteenasi olisi perustaa ryhmä yhteisesti kehitettävän asian ympärille, vaikeudet alkavat. Onneksi seitti on täynnä muitakin palveluita ja yhteistyöalustoja.

Tässä opintopiirissä pureudutaan perusasioihin, mitä kehitysalustalta vaaditaan, että ad-hoc ryhmän perustaminen on vaivatonta ja yhdessä tekeminen aika ja paikkariippumattomasti on kivaa ja tuloksekasta. Sisältönä on mm.
- Ryhmän perustaminen, ilmoittautumisten kerääminen ja jäsenten hallinta.
- Projektin yhteisten tiedostojen hallinnointi ja jakaminen.
- Keskustelu kirjoittamalla.
- Hyväksi havaittuja yhteistyökäytäntöjä ja onnistumisen esiehtoja.
- Perusasioita itsensä johtamisesta.

Ilmoittautuaksesi mukaan, tee seuraavat kaksi asiaa:

1) Ellei sinulla jo ole tunnusta GitLab.com kehitysalustalle, luo sellainen [[https://gitlab.com/users/sign_in]]. Tunnuksen saa luotua myös kirjautumalla esim. Googlen tai Twitterin tunnuksella.

2) Kirjaudu sisään ja luo uusi issue sivulla [[https://gitlab.com/TreStart-unofficial/etatyotekniikat-ilmoittautuminen/issues/new]]. Merkitse kohta "/This issue is confidential and should only be visible to team members with at least Reporter access./" ellet välttämättä halua kertoa koko maailmalle, että olet osallistumassa.

   Jos haluat kysyä jotain, niin voit tehdä sen issuen luonnin yhteydessä kirjoittamalla asiasi viestille tarkoitettuun laatikkoon tai myöhemmin kommentoimalla luomaasi issueta.

[[./issuen_luonti.png]]

Ja se oli siinä. Aikanaan ilmoittautuneet kutsutaan mukaan salaiseen projektiin, jota käytetään harjoittelussa ja jonka kautta jaetaan mm. opintopiiriä verten tuotettu materiaali.
